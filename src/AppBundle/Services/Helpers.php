<?php

namespace AppBundle\Services;

use Symfony\Component\Serializer\Serializer;

/**
 * Description of Helpers
 *
 * @author Sistemas
 */
class Helpers {

    public $manager;

    public function __construct($manager) {
        $this->manager = $manager;
    }

    public function holaMundo() {
        return "Hola mundo desde servicio 13213";
    }

    public function json($data) {
        $normalizers = array(new \Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer());
        $encoders = array("json" => new \Symfony\Component\Serializer\Encoder\JsonEncoder());
        $serializer = new Serializer($normalizers, $encoders);
        $json = $serializer->serialize($data, 'json');
        $response = new \Symfony\Component\HttpFoundation\Response();
        $response->setContent($json);
        $response->headers->set('Content-type','application/json');
        
        return $response;
    }

}
