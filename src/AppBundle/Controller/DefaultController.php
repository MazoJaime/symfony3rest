<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Services\Helpers;
use AppBundle\Services\JwAuth;

class DefaultController extends Controller {

    public function indexAction(Request $request) {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
                    'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
        ]);
    }

    public function loginAction(Request $request) {
        $helpers = $this->get(Helpers::class);
        //array data por defecto;
        $data = array(
            'status' => 'error',
            'data' => 'Send json via post'
        );
        //se recibe json por POST POR LA REQUEST
        $json = $request->get('json', NULL);
        if (NULL != $json) {
            //login
            //convertir json a objeto php
            $params = json_decode($json);

            $email = (isset($params->email)) ? $params->email : NULL;
            $password = (isset($params->password)) ? $params->password : NULL;
            $getHash = (isset($params->getHash)) ? $params->getHash : NULL;

            $emailconstraint = new Assert\Email();
            $emailconstraint->message = "Correo no valido";
            $email_valido = $this->get('validator')->validate($email, $emailconstraint);
            
            ##encriptamos contraseña
            $pwd = hash('sha256', $password);
            
            if (0 == count($email_valido) && NULL != $password && NULL != $email) {
                $jwt_auth = $this->get(JwAuth::class);
                
                if($getHash == null || $getHash == false){
                    $loged = $jwt_auth->singup($email, $pwd);
                }else{
                    $loged =$jwt_auth->singup($email, $pwd, true);
                }
                return $this->json($loged);
                
            } else {
                //fallo 
                $data = array(
                    'status' => 'error',
                    'data' => 'Email malo o password malo'
                );
            }
        }
        return $helpers->json($data);
    }

    public function pruebaAction(Request $request) {
        $helpers = $this->get(Helpers::class);
        $jwt_auth = $this->get(JwAuth::class);
        $token= $request->get("authorization",NULL);
        if($token && $jwt_auth->checktoken($token) == true){
        $em = $this->getDoctrine()->getManager();
        $userRepo = $em->getRepository("BackendBundle:User");
        $users = $userRepo->findAll();

        
        return $helpers->json(array(
                    'status' => 'success',
                    'users' => $users
        ));
        }else{
                    return $helpers->json(array(
                    'status' => 'error',
                    'data' => 'fallo al logear'
        ));
        }
    }

}
